# Resolução do exercício proposto para estágio da BySide
## Arquitetura do Sistema

### Modelo de domínio
![ModeloDominio](Docs/ModeloDominio.jpg)

### Regras de negócio
File:

  - Permissões apenas podem ser do tipo READ e WRITE.
  - A data da ultima edição é atualizada sempre que é executada uma mudança no ficheiro.

File - Root:

  - A Root do sistema de ficheiros é um Directory com parent não existente e nome "root".
  - Root não pode ser renomeada nem apagada.

File - Directory:

  - Um diretório pode estar vazio, ou seja, não ter children.

File - Document:

  - O Conteúdo de um documento pode estar vazio.

Nome:

  - Nome de um file não pode estar vazio.

### Principais Padrões Utilizados

- GRASP
- Controller Pattern
- Service Pattern
- Repository Pattern

### Principais técnicas Utilizadas
- Dependecy Injection (Manual)
- Polimorfismo
