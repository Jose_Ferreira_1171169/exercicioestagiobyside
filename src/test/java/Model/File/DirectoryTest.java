package Model.File;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DirectoryTest {
    Directory d = new Directory(null, "root", new ArrayList<>());

    @Test
    public void shouldUpdateChildren() {
        Directory dir1 = new Directory(d, "docTest", new ArrayList<>(), new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(),
                new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), new ArrayList<>());
        Document doc1 = new Document(null, "name1", "");
        List<File> lCompare = new ArrayList<>();
        lCompare.add(doc1);
        dir1.addChildren(doc1);
        assertEquals(dir1.getChildren(), lCompare);
    }

    @Test
    public void shouldBeEqualsOnSameName() {
        Directory dir1 = new Directory(null, "name1", new ArrayList<>());
        Directory dir2 = new Directory(null, "name1", new ArrayList<>());
        assertEquals(dir1, dir2);
    }

    @Test
    public void shouldBeNotEqualsOnDifferentName() {
        Directory dir1 = new Directory(null, "name1", new ArrayList<>());
        Directory dir2 = new Directory(null, "name2", new ArrayList<>());
        assertNotEquals(dir1, dir2);
    }
}
