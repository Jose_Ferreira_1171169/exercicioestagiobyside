package Model.File;

import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class FileTest {
    Directory d = new Directory(null, "root", new ArrayList<>());

    @Test
    public void shouldUpdateNameAndLastUpDate() {
        Document doc1 = new Document(d, "docTest", new ArrayList<>(), new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(),
                new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), "contentTest");
        Date dateN = Date.from(Instant.now());
        doc1.updateFileName("docTestUpdated");
        assertEquals(doc1.getFileName().getName(), "docTestUpdated");
        assertEquals(doc1.getLastUpdatedDate(), dateN);
    }

    @Test
    public void shouldUpdatePermissionsAndLastUpDate() {
        Document doc1 = new Document(d, "docTest", new ArrayList<>(), new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(),
                new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), "contentTest");
        Date dateN = Date.from(Instant.now());
        doc1.updatePermissions(new ArrayList<>());
        assertEquals(doc1.getPermissions(), new ArrayList<>());
        assertEquals(doc1.getLastUpdatedDate(), dateN);
    }

    @Test
    public void shouldUpdateParentAndLastUpDate() {
        Document doc1 = new Document(null, "docTest", new ArrayList<>(), new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(),
                new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), "contentTest");
        Date dateN = Date.from(Instant.now());
        doc1.updateParent(d);
        assertEquals(doc1.getParent(), d);
        assertEquals(doc1.getLastUpdatedDate(), dateN);
    }
}
