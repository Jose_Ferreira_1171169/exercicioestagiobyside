package Model.File;

import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DocumentTest {
    Directory d = new Directory(null, "root", new ArrayList<>());

    @Test
    public void shouldUpdateNameAndLastUpDate() {
        Document doc1 = new Document(d, "docTest", new ArrayList<>(), new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(),
                new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), "contentTest");
        Date dateN = Date.from(Instant.now());
        doc1.updateContent("docContentUpdated");
        assertEquals(doc1.getContent(), "docContentUpdated");
        assertEquals(doc1.getLastUpdatedDate(), dateN);
    }
    @Test
    public void shouldBeEqualsOnSameName() {
        Document doc1 = new Document(null,"name1","");
        Document doc2 = new Document(null,"name1","Different Content");
        assertEquals(doc1, doc2);
    }

    @Test
    public void shouldBeNotEqualsOnDifferentName() {
        Document doc1 = new Document(null,"name1","Same Content");
        Document doc2 = new Document(null,"name2","Same Content");
        assertNotEquals(doc1, doc2);
    }
}
