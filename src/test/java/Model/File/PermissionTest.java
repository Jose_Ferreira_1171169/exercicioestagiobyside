package Model.File;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PermissionTest {
    @Test
    public void shouldBeEqualsOnSameTypeAndAuth() {
        Permission p = new Permission(PermissionType.READ,true);
        Permission p2 = new Permission(PermissionType.READ,true);
        assertEquals(p, p2);
    }

    @Test
    public void shouldBeNotEqualsOnDifferentTypeOrAuth() {
        Permission p = new Permission(PermissionType.READ,true);
        Permission p2 = new Permission(PermissionType.WRITE,true);
        assertNotEquals(p, p2);
    }
}
