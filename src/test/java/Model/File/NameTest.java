package Model.File;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class NameTest {
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowErrorOnEmptyName() {
        Name n = new Name("");
    }

    @Test
    public void shouldcreateOnNotEmptyName() {
        Name n = new Name("test");
        assertEquals(n.getName(), "test");
    }

    @Test
    public void shouldBeEqualsOnSameName() {
        Name n = new Name("test");
        Name n2 = new Name("test");
        assertEquals(n, n2);
    }

    @Test
    public void shouldBeNotEqualsOnDifferentName() {
        Name n = new Name("test");
        Name n2 = new Name("test2");
        assertNotEquals(n, n2);
    }
}
