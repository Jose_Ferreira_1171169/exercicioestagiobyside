package UI;

import Bootstrap.FileBootstrap;
import Controller.FileController;
import Model.File.Directory;
import Model.File.Document;
import Model.File.File;
import Repository.BaseRepository.BaseFileRepository;
import Repository.InMemoryRepository.InMemoryFileRepository;
import Service.FileService;

public class Main {
    public static void main(String[] args) {
        try {
            FileBootstrap fileBootstrap = new FileBootstrap();
            BaseFileRepository fileRepository = new InMemoryFileRepository();
            fileBootstrap.execute();
            FileController fc = new FileController(new FileService(new InMemoryFileRepository()));
            File root = fc.getFileInfo("root");
            File r2 = fc.copyFile(root);
            Document doc1 = (Document) fc.getFileInfo("testFile1");
            doc1.updateContent("UpdatedContent");
            fc.updateFile(doc1);
            File dCreate = new Document(((Directory) root), "fileAddedTest", "contentCreate");
            File f1 = (Document) fc.createFile(dCreate);
            File fCheck = fc.getFileInfo("fileAddedTest");
            Directory workPlace = (Directory) fc.getFileInfo("workplace");
            fc.moveFile(f1, workPlace);
            fc.deleteFile("fileAddedTest");
            System.out.println("done");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
