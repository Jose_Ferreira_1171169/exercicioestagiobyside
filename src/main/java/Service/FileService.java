package Service;

import Model.File.Directory;
import Model.File.Document;
import Model.File.File;
import Repository.BaseRepository.BaseFileRepository;

public class FileService {
    public BaseFileRepository fileRep;

    public FileService(BaseFileRepository fileRepository) {
        this.fileRep = fileRepository;
    }

    public File moveFile(File fileToUpdate, Directory directoryToMove) {
        fileToUpdate.getParent().removeChildren(fileToUpdate);
        fileToUpdate.updateParent(directoryToMove);
        directoryToMove.addChildren(fileToUpdate);
        File f = this.fileRep.move(fileToUpdate, directoryToMove);
        return f;
    }

    public File createFile(File fileToCreate) {
        fileToCreate.getParent().addChildren(fileToCreate);
        return this.fileRep.create(fileToCreate);
    }

    public File getFileInfo(String fileName) {
        return this.fileRep.get(fileName);
    }

    public File updateFile(File fileToUpdate) {
        return this.fileRep.update(fileToUpdate);
    }

    public File deleteFile(String fileName) {
        if (!fileName.equals("root")) {
            File f = this.fileRep.get(fileName);
            f.getParent().removeChildren(f);
            return this.fileRep.delete(fileName);
        }
        return null;
    }

    public File copyFile(File fileToCopy) {
        File fCopy;
        if (fileToCopy instanceof Directory) {
            fCopy = new Directory(((Directory) fileToCopy));
        } else {
            fCopy = new Document(((Document) fileToCopy));
        }
        return this.fileRep.create(fCopy);
    }
}
