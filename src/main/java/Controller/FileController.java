package Controller;

import Model.File.Directory;
import Model.File.File;
import Model.File.PermissionType;
import Service.FileService;

public class FileController {
    private FileService fileSv;

    public FileController(FileService fileSv) {
        this.fileSv = fileSv;
    }

    public File createFile(File fileToCreate){
            return this.fileSv.createFile(fileToCreate);
    }

    public File getFileInfo(String fileName) throws IllegalArgumentException, IllegalAccessException {
        File f = this.fileSv.getFileInfo(fileName);
        if ((f.hasPermission(PermissionType.READ))) {
            return this.fileSv.getFileInfo(fileName);
        } else {
            throw new IllegalAccessException("Não tem as permissões necessárias");
        }
    }

    public File updateFile(File fileToUpdate) throws IllegalArgumentException, IllegalAccessException {
        if (fileToUpdate.hasPermission(PermissionType.WRITE)) {
            return this.fileSv.updateFile(fileToUpdate);
        } else {
            throw new IllegalAccessException("Não tem as permissões necessárias");
        }
    }

    public File deleteFile(String fileName) throws IllegalArgumentException, IllegalAccessException {
        File f = this.fileSv.getFileInfo(fileName);
        if (f.hasPermission(PermissionType.WRITE)) {
            return this.fileSv.deleteFile(fileName);
        } else {
            throw new IllegalAccessException("Não tem as permissões necessárias");
        }
    }

    public File copyFile(File fileToCopy) throws IllegalArgumentException, IllegalAccessException {
        if (fileToCopy.hasPermission(PermissionType.WRITE)) {
            return this.fileSv.copyFile(fileToCopy);
        } else {
            throw new IllegalAccessException("Não tem as permissões necessárias");
        }
    }

    public File moveFile(File fileToUpdate, Directory directoryToMove) throws IllegalArgumentException, IllegalAccessException {

        if (fileToUpdate.hasPermission(PermissionType.WRITE)) {
            return this.fileSv.moveFile(fileToUpdate, directoryToMove);
        } else {
            throw new IllegalAccessException("Não tem as permissões necessárias");
        }
    }

}
