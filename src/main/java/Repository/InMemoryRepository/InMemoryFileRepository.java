package Repository.InMemoryRepository;

import Model.File.Directory;
import Model.File.File;
import Model.File.Name;
import Repository.BaseRepository.BaseFileRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryFileRepository implements BaseFileRepository {
    private static List<File> inMemoryList = new ArrayList<>();

    @Override
    public File move(File file, Directory directory) {
        update(directory);
        return update(file);
    }

    @Override
    public File create(File file) {
        if (file.getParent() == null && file.getFileName().getName() != "root") { // allow 1 root only
            throw new IllegalArgumentException("File tem de ter parent");
        }
        for (File f : inMemoryList) {
            if (f.getFileName().equals(file.getFileName())) {
                throw new IllegalArgumentException("Nome já existente");
            }
        }
        inMemoryList.add(file);
        return file;
    }

    @Override
    public File update(File file) {
        for (File f : inMemoryList) {
            if (file.equals(f)) {
                inMemoryList.remove(f);
                inMemoryList.add(file);
                return file;
            }
        }
        return null;
    }

    @Override
    public File rename(Name oldName, Name newName) {
        if (oldName.getName() == "root") {
            throw new IllegalStateException("root não pode ser renamed");
        }
        for (File f : inMemoryList) {
            if (f.getFileName().equals(oldName)) {
                f.updateFileName(newName);
                return f;
            }
        }
        return null;
    }

    @Override
    public File get(String name) {
        for (File f : inMemoryList) {
            if (f.getFileName().getName().equals(name)) {
                return f;
            }
        }
        return null;
    }

    @Override
    public List<File> getAll() {
        return inMemoryList;
    }

    @Override
    public File delete(String name) {
        if (name == "root") {
            throw new IllegalStateException("root não pode ser eliminada");
        }
        for (File f : inMemoryList) {
            if (f.getFileName().getName().equals(name)) {
                inMemoryList.remove(f);
                return f;
            }
        }
        return null;
    }
}
