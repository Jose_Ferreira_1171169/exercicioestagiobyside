package Repository.MockRepository;

import Model.File.*;
import Repository.BaseRepository.BaseFileRepository;

import java.util.ArrayList;
import java.util.List;

public class MockFileRepository implements BaseFileRepository {
    File root;
    File f;

    public MockFileRepository() {
        List<Permission> pList = new ArrayList<>();
        Permission pR = new Permission(PermissionType.READ, true);
        Permission pW = new Permission(PermissionType.WRITE, true);
        root = new Directory(null, "root", new ArrayList<>());
        root.updatePermissions(pList);
        f = new Document(((Directory) root), "fTest", "contentTest");
        f.updatePermissions(pList);
        ((Directory) root).addChildren(f);
    }

    @Override
    public File move(File file, Directory directory) {
        if (file.getFileName().equals(f.getFileName())) {
            return file;
        } else {
            return null;
        }
    }

    @Override
    public File rename(Name oldName, Name newName) {
        if (oldName.equals(f.getFileName())) {
            return f;
        }
        return null;
    }

    @Override
    public File create(File obj) {
        if (!obj.getFileName().equals(f.getFileName())) {
            return obj;
        } else {
            return null;
        }
    }

    @Override
    public File update(File obj) {
        if(obj.getFileName().equals(f.getFileName())){
            return obj;
        }else{
            return null;
        }
    }

    @Override
    public File get(String id) {
        if (id.equals(f.getFileName().getName())) {
            return f;
        } else {
            return null;
        }
    }

    @Override
    public List<File> getAll() {
        List<File> fileList = new ArrayList<>();
        fileList.add(f);
        fileList.add(root);
        return fileList;
    }

    @Override
    public File delete(String id) {
        if (id.equals(f.getFileName().getName())) {
            return f;
        } else {
            return null;
        }
    }
}
