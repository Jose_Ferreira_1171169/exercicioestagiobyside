package Repository.BaseRepository;

import Model.File.Directory;
import Model.File.File;
import Model.File.Name;

public interface BaseFileRepository extends BaseRepository<File> {
    File move(File file, Directory directory);
    File rename(Name oldName, Name newName);
}
