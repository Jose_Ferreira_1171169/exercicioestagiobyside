package Repository.BaseRepository;

import java.util.List;

public interface BaseRepository<T> {
    T create(T obj);

    T update(T obj);

    T get(String id);

    List<T> getAll();

    T delete(String id);
}
