package Bootstrap;

import Model.File.Directory;
import Model.File.Document;
import Model.File.Permission;
import Model.File.PermissionType;
import Repository.BaseRepository.BaseFileRepository;
import Repository.InMemoryRepository.InMemoryFileRepository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileBootstrap {
    public FileBootstrap() {}

    public void execute() {
        Permission pR = new Permission(PermissionType.READ, true);
        Permission pW = new Permission(PermissionType.WRITE, true);
        List<Permission> permissionList = new ArrayList<>();
        permissionList.add(pR);
        permissionList.add(pW);
        Directory root = new Directory(null, "root", new ArrayList<>(), Date.from(Instant.now()), Date.from(Instant.now()), new ArrayList<>());
        Document doc1 = new Document(root, "testFile1", permissionList, Date.from(Instant.now()), Date.from(Instant.now()), "testFileContent1");
        Directory d2 = new Directory(root, "workplace", permissionList, Date.from(Instant.now()), Date.from(Instant.now()), new ArrayList<>());
        root.addChildren(doc1);
        root.addChildren(d2);
        Document doc2 = new Document(d2, "testFile2", permissionList, Date.from(Instant.now()), Date.from(Instant.now()), "testFileContent2");
        d2.addChildren(doc2);
        BaseFileRepository fRep = new InMemoryFileRepository();
        fRep.create(root);
        fRep.create(d2);
        fRep.create(doc1);
        fRep.create(doc2);
    }
}
