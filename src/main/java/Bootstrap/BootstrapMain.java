package Bootstrap;


public class BootstrapMain {

    public static class AppRun {

        public static void main(String[] args) {
            try {
                runBootStrap();
            } catch (Exception e) {
                System.exit(1);
            }
            System.exit(0);
        }

        public static void runBootStrap() {
            new FileBootstrap().execute();
        }
    }
}
