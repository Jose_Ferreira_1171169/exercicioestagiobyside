package Model.File;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Directory extends File {

    private List<File> children;

    public Directory(Directory d) {
        super(d);
        List<File> fList = new ArrayList<>();
        for (File f : d.children) {
            File fCopy;
            if (f instanceof Directory) {
                fCopy = new Directory(((Directory) f));
            } else {
                fCopy = new Document(((Document) f));
            }
            fList.add(fCopy);
        }
        this.children = fList;
    }

    public Directory(Directory parent, String fileName, List<Permission> permissions,
                     Date createdDate, Date lastUpdatedDate, List<File> children) {
        super(parent, fileName, permissions, createdDate, lastUpdatedDate);
        this.children = children;
    }

    public Directory(Directory parent, String fileName, List<File> children) {
        super(parent, fileName);
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Directory) {
            return this.fileName.equals(((Directory) o).fileName);
        }
        return false;
    }

    public boolean addChildren(File f) {
        return this.children.add(f);
    }

    public boolean removeChildren(File f) {
        return this.children.remove(f);
    }
    public List<File> getChildren(){
        return this.children;
    }

    @Override
    public int hashCode() {
        return this.fileName.hashCode();
    }
}
