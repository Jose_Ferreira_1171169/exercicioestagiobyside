package Model.File;

public class Permission {

    private PermissionType pType;
    private boolean auth;

    public Permission(Permission p){
        this.pType = p.pType;
        this.auth = p.auth;
    }
    public Permission(PermissionType pType, boolean auth) {
        this.pType = pType;
        this.auth = auth;
    }

    public boolean hasPermission() {
        return auth;
    }

    public PermissionType getPermissionType() {
        return this.pType;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Permission) {
            Permission oPerm = (Permission) o;
            return this.pType.equals(oPerm.pType) && this.auth == oPerm.auth;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.pType.hashCode();
    }
}
