package Model.File;

public class Name {
    private String name;

    public Name(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Nome não pode estar vazio");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Name) {
            return this.name.equals(((Name) o).name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
