package Model.File;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class File {

    protected Directory parent;
    protected Name fileName;
    protected List<Permission> permissions;
    protected Date createdDate;
    protected Date lastUpdatedDate;

    protected File(File fToCopy) {
        this.parent = fToCopy.parent;
        this.fileName = new Name((fToCopy.fileName.getName()+"Copy"));
        List<Permission> pList = new ArrayList<>();
        for (Permission p : fToCopy.permissions) {
            pList.add(new Permission(p));
        }
        this.permissions = pList;
        this.createdDate = Date.from(Instant.now());
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public File(Directory parent, String fileName, List<Permission> permissions,
                Date createdDate, Date lastUpdatedDate) {
        this.parent = parent;
        this.fileName = new Name(fileName);
        this.permissions = permissions;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public File(Directory parent, String fileName) {
        Permission pR =  new Permission(PermissionType.READ,false);
        Permission pW =  new Permission(PermissionType.WRITE,false);
        List<Permission> permissionList = new ArrayList<>();
        permissionList.add(pR);
        permissionList.add(pW);
        this.parent = parent;
        this.fileName = new Name(fileName);
        this.permissions = permissionList;
        this.createdDate = Date.from(Instant.now());
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public void updateFileName(String fileName) {
        this.fileName = new Name(fileName);
        this.lastUpdatedDate = Date.from(Instant.now());
    }
    public void updateFileName(Name fileName) {
        this.fileName = fileName;
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public Name getFileName() {
        return this.fileName;
    }

    public void updateParent(Directory d) {
        this.parent = d;
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public Directory getParent() {
        return this.parent;
    }

    public void updatePermissions(List<Permission> permissions) {
        this.permissions = permissions;
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public List<Permission> getPermissions() {
        return this.permissions;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public Date getLastUpdatedDate() {
        return this.lastUpdatedDate;
    }

    public boolean hasPermission(PermissionType pt) {
        for (Permission p : permissions) {
            if (p.getPermissionType().toString() == pt.toString()) {
                return p.hasPermission();
            }
        }
        return false;
    }

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();
}
