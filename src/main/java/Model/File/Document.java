package Model.File;


import java.time.Instant;
import java.util.Date;
import java.util.List;

public class Document extends File {
    private String content;

    public Document(Document d) {
        super(d);
        this.content = d.content;
    }

    public Document(Directory parent, String fileName, List<Permission> permissions,
                    Date createdDate, Date lastUpdatedDate, String content) {
        super(parent, fileName, permissions, createdDate, lastUpdatedDate);
        this.content = content;
    }

    public Document(Directory parent, String fileName,
                    String content) {
        super(parent, fileName);
        this.content = content;
    }

    public void updateContent(String newContent) {
        this.content = newContent;
        this.lastUpdatedDate = Date.from(Instant.now());
    }

    public String getContent() {
        return this.content;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Document) {
            return this.fileName.equals(((Document) o).fileName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.fileName.hashCode();
    }
}
